<!-- ANCHOR: header -->

# Inglish

A strictly regular language inspired by English.

<!-- ANCHOR_END: header -->

See [web version](https://dpdmancul.gitlab.io/inglish/).

<!-- ANCHOR: content -->

## License

<center><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a></center><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

The source files from which this book is generated can be found on [GitLab](https://gitlab.com/DPDmancul/inglish).

<!-- ANCHOR_END: content -->

