# Verbs

## Infinitive

The infinitive is composed by _tu_, followed by the base form of the verb (e.g. _tu bi_, to be).

## Indicative

The present tense of the indicative is equal to the base form of the verb following the subject.

| Subject     | Verb |
|:------------|:-----|
| ay          | bi   |
| yu          | bi   |
| hi, shi, it | bi   |
| wi          | bi   |
| yu          | bi   |
| dey         | bi   |

In the past the base form is preceded by _did_, while in the future by _wil_.

In the negative form there is a _not_ after _du_, _did_ and _wil_, whilst in the interrogative form these auxiliaries are in front of the sentence.

|               | Present      | Past          | Future        |
|:--------------|:-------------|:--------------|:--------------|
| Affermative   | Ay bi        | Ay did bi     | Ay wil bi     |
| Negative      | Ay du not bi | Ay did not bi | Ay wil not bi |
| Interrogative | Du ay bi?    | Did ay bi?    | Wil ay bi?    |

## Conditional

TODO

## Imperative

The imperative exists only for the second person (_yu_) and it is equal to the base form of the verb (e.g. _go_), preceded by _du not_ for the negative form (e.g. _du not go_).

### Volitive

The volitive is composed by _let_, the object and the base form of the verb (e.g. _let Jon si_, let John see).

## Participle

The present participle is composed by the base form followed by -er (e.g. _rayter_, writer), whilst the past participle is followed by -ed (e.g. _rayted_, written).

### Passive voice

The passive is composed by the indicative of to be and the past participle of the verb (e.g. _de leter did bi rayted_, the letter was written)

## Modal verbs 

The modal verbs (_wont_, _layk_, _kan_, ...) are always followed by the base form (e.g. _ay wont rayt_, I want to write).
