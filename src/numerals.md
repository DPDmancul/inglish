# Numerals

## Cardinal numerals

### Up to 10

| Number | Numeral |
|:------:|:-------:|
|    0   |   ziro  |
|    1   |   wan   |
|    2   |   chu   |
|    3   |   tri   |
|    4   |   for   |
|    5   |   fayv  |
|    6   |   siks  |
|    7   |  seven  |
|    8   |   eit   |
|    9   |   nayn  |
|   10   |   ten   |

### From 11 to 19

Simply say 10 and the units:

| Number |  Numeral  |
|:------:|:---------:|
|   11   |  ten wan  |
|   12   |  ten chu  |
|   13   |  ten tri  |
|   14   |  ten for  |
|   15   |  ten fayv |
|   16   |  ten siks |
|   17   | ten seven |
|   18   |  ten eit  |
|   19   |  ten nayn |

### From 20 to 99

#### Tens

Follow the tens by 10:

| Number |  Numeral  |
|:------:|:---------:|
|   20   |  chu ten  |
|   30   |  tri ten  |
|   40   |  for ten  |
|   50   |  fayv ten |
|   60   |  siks ten |
|   70   | seven ten |
|   80   |  eit ten  |
|   90   |  nayn ten |

#### Others

Join the tens and the unit. For example:

| Number |    Numeral    |
|:------:|:-------------:|
|   25   |  chu ten fayv |
|   72   | seven ten chu |

### After 99

The schema is always the same:

| Number |                Numeral               |
|:------:|:------------------------------------:|
|   100  |                handred               |
|   123  |          handred chu ten tri         |
|   234  |        chu handred tri ten for       |
|  1000  |                tauzend               |
|  1234  |    tauzend chu handred tri ten for   |
|  2345  | chu tauzend tri handred for ten fayv |

## Ordinal numerals

Ordinals are formed by the cardinal and the -t suffix.

For the 8 which already ends in -t the suffix is -it (like the plural suffix -is for words which end in -s or similar).

| Number |            Numeral            |
|:------:|:-----------------------------:|
|   1ᵗ   |             wan*t*            |
|   2ᵗ   |             chu*t*            |
|   3ᵗ   |             tri*t*            |
|   4ᵗ   |             for*t*            |
|   5ᵗ   |            fayv*t*            |
|   6ᵗ   |            siks*t*            |
|   7ᵗ   |            seven*t*           |
|   8ᵗ   |            eit*it*            |
|   9ᵗ   |            nayn*t*            |
|   10ᵗ  |             ten*t*            |
|   15ᵗ  |          ten fayv*t*          |
|   36ᵗ  |        tri ten siks*t*        |
|  698ᵗ  | siks handred nayn ten eit*it* |


