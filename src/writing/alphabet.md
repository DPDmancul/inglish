# Alphabet

| Letter |  Name  |
|:------:|:------:|
|    a   |    a   |
|    b   |   bi   |
|    c   |   chi  |
|    d   |   di   |
|    e   |    e   |
|    f   |   ef   |
|    g   |   gi   |
|    h   |  eych  |
|    i   |    i   |
|    j   |   jey  |
|    k   |   key  |
|    l   |   el   |
|    m   |   em   |
|    n   |   en   |
|    o   |    o   |
|    p   |   pi   |
|    r   |   ar   |
|    s   |   es   |
|    t   |   ti   |
|    u   |    u   |
|    v   |   vi   |
|    w   | dablyu |
|    y   |   way  |
|    z   |   zed  |


## Other letters

| Letter | Name |
|:------:|:----:|
|    q   |  kyu |
|    x   |  eks |


