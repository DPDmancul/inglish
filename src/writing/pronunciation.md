# Pronunciation

## Vowels

Inglish has only 5 vowels, compared to the many of English.  
In this table also the other English vowels are listed, in the same line of the nearest Inglish one.

| Letter | Pronunciation | Other English vowels |
|:------:|:-------------:|:--------------------:|
|    a   |      /a/      |     /æ/, /ɑ/, /ɐ/    |
|    e   |      /e/      |          /ε/         |
|    i   |      /i/      |          /ɪ/         |
|    o   |      /o/      |          /ɔ/         |
|    u   |      /u/      |          /ʊ/         |

## Semivowels

The semivowels are the same of English.

| Letter | Pronunciation |
|:------:|:-------------:|
|    y   |      /j/      |
|    w   |      /w/      |

Y and w are used also for non-syllabic i and u, respectively.

## Consonants

The consonants are quite the same of English, but each letter / digraph represents only one sound.

| Letter / Digraph | Pronunciation | Other English consonants |
|:----------------:|:-------------:|:------------------------:|
|         b        |      /b/      |                          |
|        ch        |      /tʃ/     |                          |
|         d        |      /d/      |            /ð/           |
|         f        |      /f/      |                          |
|         g        |      /g/      |                          |
|         h        |      /h/      |                          |
|         j        |      /dʒ/     |                          |
|         k        |      /k/      |            /x/           |
|         l        |      /l/      |                          |
|         m        |      /m/      |                          |
|         n        |      /n/      |            /ŋ/           |
|         p        |      /p/      |                          |
|         r        |      /r/      |                          |
|         s        |      /s/      |                          |
|        sh        |      /ʃ/      |                          |
|         t        |      /t/      |            /θ/           |
|         v        |      /v/      |                          |
|         z        |      /z/      |                          |
|        zh        |      /ʒ/      |                          |
