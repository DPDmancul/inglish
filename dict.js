"use strict";

function inglish_dict_search() {
  let req = document.getElementById("inglish-dict-search").value;
  let res = document.getElementById("inglish-dict-results");
  let entries = inglish_dict_index.search(req, {
    expand: true
  });
  res.innerHTML = "";
  if (entries.length !== 0) {
    res.appendChild(inglish_dict_head);
    let tbody = document.createElement('tbody');
    for (let entry of entries) {
      let eng = document.createElement('td');
      eng.innerHTML = entry.doc.eng;
      let ing = document.createElement('td');
      ing.innerHTML = entry.doc.ing;
      let row = document.createElement('tr');
      row.append(eng);
      row.append(ing);
      tbody.append(row);
    }
    res.appendChild(tbody);
  }
}

var inglish_dict_index;
var inglish_dict_head;

if (document.getElementById("inglish-dict-search")) {
  let head_eng = document.createElement('th');
  head_eng.innerHTML = "English";
  let head_ing = document.createElement('th');
  head_ing.innerHTML = "Inglish";
  let head_row = document.createElement('tr');
  head_row.append(head_eng);
  head_row.append(head_ing);
  inglish_dict_head = document.createElement('thead');
  inglish_dict_head.append(head_row);

  inglish_dict_index = new elasticlunr.Index;
  inglish_dict_index.pipeline.add(
    elasticlunr.trimmer,
    elasticlunr.stemmer
  );
  inglish_dict_index.addField('eng');
  inglish_dict_index.addField('ing');

  [
    // A
    ["a", "a"],
    ["about", "abaut"],
    ["after", "after"],
    ["all", "ol"],
    ["also", "olso"],
    ["an", "a"],
    ["any", "eni"],
    ["and", "and"],
    ["as", "az"],
    ["at", "at"],

    // B
    ["back", "bak"],
    ["be", "bi"],
    ["because", "bikoz"],
    ["but", "bat"],
    ["by", "bay"],

    // C
    ["can", "kan"],
    ["cat", "kat"],
    ["come", "cam"],

    // D
    ["day", "dey"],
    ["did", "did"],
    ["do", "du"],
    ["dog", "dog"],

    //
    ["even", "iven"],

    // F
    ["for", "for"],
    ["from", "from"],

    // G
    ["get", "get"],
    ["give", "giv"],
    ["go", "go"],
    ["good", "gud"],

    // H
    ["have", "hav"],
    ["he", "hi"],
    // ["her", "TODO"], // her or a more regular choice?
    ["hello", "hello"],
    // ["his", "TODO"], // his or a more regular choice?
    ["how", "haw"],

    // I
    ["I", "ay"],
    ["if", "if"],
    ["in", "in"],
    ["into", "intu"],
    ["it", "it"],

    // J
    ["John", "Jon"],
    ["just", "jast"],

    // K
    ["know", "now"],

    // L
    ["like", "layk"],
    ["less", "les"],
    ["letter", "leter"],
    ["let", "let"],
    ["look", "luk"],

    // M
    ["make", "meyk"],
    ["measure", "mezhur"],
    ["more", "mor"],
    ["most", "most"],
    // ["my", "TODO"], // may or a more regular choice?

    // N
    ["new", "nju"],
    ["no", "no"],
    ["not", "not"],
    ["now", "naw"],

    // O
    ["of", "ov"],
    ["on", "on"],
    ["one", "wan"],
    ["only", "onli"],
    ["or", "or"],
    ["out", "aut"],
    ["other", "ader"],
    ["over", "over"],

    // P
    ["people", "persons"],
    ["person", "person"],

    // S
    ["same", "seym"],
    ["say", "sey"],
    ["see", "si"],
    ["she", "shi"],
    ["so", "so"],
    ["some", "sam"],

    // T
    ["take", "teyk"],
    ["than", "dan"],
    ["that", "dat"],
    ["the", "de"],
    ["these", "diz"],
    // ["their", "TODO"], // deyr or a more regular choice?
    ["then", "den"],
    ["there", "der"],
    ["they", "dey"],
    ["this", "dis"],
    ["think", "tink"],
    ["time", "taym"],
    ["to", "tu"],
    ["two", "chu"],

    // U
    ["up", "ap"],
    ["use", "yuz"],

    // W
    ["want", "wont"],
    ["way", "wey"],
    ["we", "wi"],
    ["what", "wot"],
    ["when", "wen"],
    ["which", "wich"],
    ["who", "hu"],
    ["will", "wil"],
    ["with", "wid"],
    ["work", "work"],
    ["write", "rayt"],

    // Y
    ["year", "yiar"],
    ["you", "yu"],
  ].forEach(function (entry, id) {
    inglish_dict_index.addDoc({
      id,
      eng: entry[0],
      ing: entry[1]
    })
  });
  console.log("Dictionary loaded");
}

